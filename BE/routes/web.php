<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SongController;
use App\Http\Controllers\PlaylistController;
use App\Http\Controllers\PlaylistSongController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Songs route
Route::get('/songs', [SongController::class, 'displaySongs']);
Route::post('/upload', [SongController::class, 'store']);
//Playlist route
Route::get('/playlist', [PlaylistController::class, 'displayPlaylists']);
Route::post('/create', [PlaylistController::class, 'store']);
//PlaylistSong route
Route::get('/playlistsongs', [PlaylistSongController::class, 'displayPlaylistSongs']);
Route::post('/create', [PlaylistSongController::class, 'store']);