<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Song;
class SongController extends Controller
{
public function displaySongs(){
    return DB::table('songs')->get();
}

//Upload File
public function store(Request $request){

    $newSongs = new Songs();
    $newSongs->title = $request->title;
    $newSongs->length = $request->length;
    $newSongs->artist = $request->artist;
    $newSongs->save();
    return $newSongs;
}
}