<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlist;
class PlaylistController extends Controller
{
public function displayPlaylist(){
    return DB::table('playlist')->get();
}

//Upload File
public function store(Request $request){

    $newPlaylist = new Playlist();
    $newPlaylist->title = $request->title;
    $newPlaylist->length = $request->length;
    $newPlaylist->artist = $request->artist;
    $newPlaylist->save();
    return $newPlaylist;
}
}
